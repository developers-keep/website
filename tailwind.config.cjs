/** @type {import('tailwindcss').Config}*/
const config = {
	content: [
		'./src/**/*.{html,js,svelte,ts}',
		'./node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'
	],

	plugins: [require('flowbite/plugin')],

	darkMode: 'class',

	theme: {
		extend: {
			colors: {
				// flowbite-svelte
				primary: {
					50: '#DCE7E8',
					100: '#A7DCDC',
					200: '#739594',
					300: '#6EC2C4',
					400: '#3AA9AE',
					500: '#3c8a7d',
					600: '#018c8f',
					700: '#008b9c',
					800: '#096069',
					900: '#004a49'
				}
			}
		}
	}
};

module.exports = config;
