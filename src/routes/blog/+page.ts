/* eslint-disable @typescript-eslint/no-explicit-any */
// noinspection JSUnusedGlobalSymbols
export async function load() {
  const regex = /\/src\/blog\/content\/(.*)\.svelte\.md$/;
  const posts = import.meta.glob('/src/blog/content/*.svelte.md');
  const previews = [];
  for (const slug of Object.keys(posts)) {
    const data: any = await posts[slug]();
    const match = regex.exec(slug);
    if (!match) throw new Error(`Invalid slug: ${slug}`);
    previews.push({
      ...(data.metadata),
      slug: match[1],
    });
  }
  return { previews };
}