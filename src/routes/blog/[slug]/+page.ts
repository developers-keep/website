import type { LoadEvent } from "@sveltejs/kit";

// noinspection JSUnusedGlobalSymbols
export async function load({ params }: LoadEvent) {
  const { slug } = params;
  try {
    const data = await import(`../../../blog/content/${slug}.svelte.md`);
    const content = data.default;
    return {
      status: 200,
      post: {
        content,
        slug,
        ...data.metadata
      }
    };
  } catch (e) {
    return { status: 404 };
  }
}